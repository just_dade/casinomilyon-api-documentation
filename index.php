<?php include "inc/config.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<title>CasinoMilyon API</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/shCore.js"></script>
	<script type="text/javascript" src="js/shBrushJScript.js"></script>
	<script type="text/javascript" src="js/shBrushPlain.js"></script>
	<link href="css/shCore.css" rel="stylesheet" type="text/css" />
	<link href="css/shThemeRDark.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<header>
	<h1>CasinoMilyon API</h1>
</header>

<aside id="sidebar">
	<ul>
		<?php $q = $sq->query("SELECT * FROM method"); while ($method = $q->fetch_assoc()) { ?>
		<li><a href="#<?php echo $method['machine_title']; ?>"><?php echo $method['link_title']; ?></a></li>
		<?php } ?>
	</ul>
</aside>

<div id="methods">

	<?php $q = $sq->query("SELECT * FROM method"); while ($method = $q->fetch_assoc()) { ?>
	<?php
			$met_id = $method['id'];
			$met_title = $method['title'];
			$met_title_link = $method['link_title'];
			$met_title_machine = $method['machine_title'];
			$met_description = $method['description'];
			$met_arguments = $method['arguments'];
			$met_output = $method['output'];
			$met_responses = $method['response'];
	?>
		<div class="method" id="<?=$met_title_machine?>">
			<div class="method-body">
				<section class="method-description">
				<?php
					echo '<h2>'.$met_title.'</h2>';
					echo $met_description;

					if ($met_arguments != 0) {
						include 'inc/arguments.php';
					}

					if ($met_output != 0) {
						include 'inc/output.php';
					}
				?>
			</section>
			<section class="method-example">

				<?php
					if ($met_responses != 0) {
						include 'inc/responses.php';
					}
				?>
			</section>
			</div>
		</div>
	<?php } ?>
</div>

<script type="text/javascript">
     SyntaxHighlighter.all();
     $(window).on('scroll', function(e) {
     	var pos = $(this).scrollTop();
     	console.log(pos);
     	if (pos > 50) {
     		$('#sidebar').css('top', '0');
     	} else if (pos > 0 && pos < 50) {
     		$('#sidebar').css('top', 50-pos+'px');
     	} else {
     		$('#sidebar').css('top', '50px');
     	}
     });

     $('#sidebar li').on('click', function() {
     	if (!$(this).hasClass('active')) {
     		$('#sidebar li.active').removeClass('active');
     		$(this).addClass('active');
     	}
     });
</script>

</body>
</html>