<h3>Output</h3>
<table class="arguments">
<?php
	$q4 = $sq->query("SELECT * FROM output WHERE method_id='$met_id'");
	while ($argument = $q4->fetch_assoc()) {
			$arg_id = $argument['id'];
			$arg_method_id = $argument['method_id'];
			$arg_title = $argument['title'];
			$arg_def = $argument['default'];
			$arg_description = $argument['description'];
			$arg_possible = $argument['possible'];

		echo '<tr>';
		echo '<td>'.$arg_title.'</td>';
		echo '<td>';
			if ($arg_def) {
				echo ', <i>'.$arg_def.'</i>';
			}
			if (!empty($arg_description)) {
				echo $arg_description;
			}
			if ($arg_possible != 0) {
				echo '<strong>possible:</strong> ';
				include 'possible_output.php';
			}
		echo '</td>';
	}
?>
</table>