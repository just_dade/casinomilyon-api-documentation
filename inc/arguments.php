<h3>Arguments</h3>
<table class="arguments">
<?php
	$q2 = $sq->query("SELECT * FROM arguments WHERE method_id='$met_id'");
	while ($argument = $q2->fetch_assoc()) {
			$arg_id = $argument['id'];
			$arg_method_id = $argument['method_id'];
			$arg_title = $argument['title'];
			$arg_def = $argument['default'];
			$arg_description = $argument['description'];
			$arg_possible = $argument['possible'];

		if ($argument['required'] == 0) { $arg_required = 'optional'; } else { $arg_required = 'required'; }

		echo '<tr>';
		echo '<td>'.$arg_title.'</td>';
		echo '<td>';
			echo '<strong>'.$arg_required.'</strong>';
			if ($arg_def) {
				echo ', <i>'.$arg_def.'</i>';
			}
			echo '<p>'.$arg_description.'</p>';
			if ($arg_possible != 0) {
				echo '<p><strong>possible:</strong> ';
					include 'possible.php';
				echo '</p>';
			}
		echo '</td>';
	}
?>
</table>