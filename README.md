# CasinoMilyon PS API #

![apiscr.png](https://bitbucket.org/repo/qAaxbx/images/4140853155-apiscr.png)

### Demo? ###

* [Here you go!](http://dade.us/api)

### SQL Dump? ###

* [Here you go!](http://dade.us/api/cmapi.sql)

### Config? ###

* inc/config.php

### Progress? ###

* 26/73 Methods Parsed

Enjoy!